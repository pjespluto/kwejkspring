package com.example.kwejk;


import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Controller
public class CategoryController {
    @GetMapping("/categories")
    public String getCategories(ModelMap map) {
        map.put("categories", Category.getCategories());
        return "categories";
    }

    @GetMapping("/category/{id}")
    public String showCategory(@PathVariable int id, ModelMap modelMap){
        modelMap.put("gifs",Gif.getGifsByCategoryId(id));
        return "category";
    }
}

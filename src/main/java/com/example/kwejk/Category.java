package com.example.kwejk;

import java.util.ArrayList;
import java.util.List;

public class Category {

    private int id; // liczba ktora identyfikuje kategorie

    private String name; // nazwa kategorii

    // public String name;

    private static int counter = 1;

    public Category(String name) {
        this.name = name;
        this.id = counter++;
        // = ma wiekszy priorytet niz ++
        // najpierw wykonywana jest this.id=counter, a pozniej counter=counter++

    }

    private static List<Category> categories = new ArrayList<>();

    static {
        categories.add(new Category("android"));
        categories.add(new Category("bot"));
        categories.add(new Category("funny"));
    }

    public static Category findByCategoryId(int id){
        for ( Category c:categories){
            if (c.getId()==id){
                return c;
            }
        }
        return null;
    }


    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public static List<Category> getCategories() {
        return categories;
    }
}

package com.example.kwejk;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Controller
public class HomeController {

    @GetMapping("/")
    public String home(ModelMap map){
        map.put("gifs",Gif.getGifs());
        return "home";
    }
    @GetMapping("/favorites")
    public String getFavorites(ModelMap map){
        map.put("gifs", Gif.getFavorites());
        return "favorites";

    }

    // gif/nazwa_gifu

    @GetMapping ("/gif/{name}")
    public String show(@PathVariable String name, ModelMap map){
        map.put("gif", Gif.findGifByName(name));
        return "gif-details";
    }
}

package com.example.kwejk;

import java.util.ArrayList;
import java.util.List;

public class Gif {
    private String name;

    private boolean favorite;
    private int categoryId;

    private int categoryId() {
        return categoryId;
    }

    public boolean getFavorite() {
        return favorite;
    }

    public void setFavorite(boolean favorite) {
        this.favorite = favorite;
    }

    public Gif(String name) {
        this.name = name;
    }

    public Gif(String name, boolean favorite) {
        this.name = name;
        this.favorite = favorite;


    public Gif(int categoryId) {
        this.categoryId = categoryId;
    }

    public Gif(String name, boolean favorite, int categoryId) {
        this.name = name;
        this.favorite = favorite;
        this.categoryId = categoryId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private static List<Gif> gifs = new ArrayList<>();

    public static List<Gif> getGifs() {
        return gifs;
    }

    public static List<Gif> getFavorites() {
        List<Gif> favorites = new ArrayList<>();
        for (Gif gif : getGifs()) {
            if (gif.getFavorite())
                favorites.add(gif);
        }
        return favorites;
    }

    public static Gif findGifByName(String name) {
        Gif gif = null;

        for (Gif g : getGifs()) {
            if (g.getName().equals(name)) {
                gif = g;
            }
        }
        return gif;
    }

    static {
        gifs.add(new Gif("ben-and-mike", true, 1));
        gifs.add(new Gif("android-explosion", false, 2));
        gifs.add(new Gif("book-dominos", true, 1));
        gifs.add(new Gif("compiler-bot", false, 2));
        gifs.add(new Gif("cowboy-coder", false, 2));
        gifs.add(new Gif("infinite-andrew", false, 2));
    }
    }     public static List<Gif> getGifsByCategoryId(int id) {
        List<Gif> gifs = new ArrayList<>();

        for (Gif gif : gifs){
            if (gif.getCategoryId() == id) {
                gifCategories.add(gif);
            }
            return gifCategories;
        }




    // static
    public String getFilePath() {
        return "gifs/" + getName() + ".gif";

    }

    public String getPath() {
        return "/gif/" + getName();
    }

    public String getClassCss() {
        if (getFavorite()) {
            return "mark favorite";
        } else {
            return "un favorite";
        }
    }
}
